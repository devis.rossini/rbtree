#include "rbtree_basic_test.h"
#include "rbtree.h"

#include <iostream>

void BasicTest()
{
	rbt::rbtree<size_t, size_t> t1;
	for (size_t i = 0; i < 51; ++i)
		t1.insert(i, i);

	t1.walk(rbt::Show<size_t, size_t>(std::cout, "\n"));

	std::cout << std::endl;

	size_t keys[] = { 10, 85, 15, 70, 20, 60, 30, 50, 65, 80,
		90, 40, 5, 55 };
	size_t values[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
	rbt::rbtree<size_t, size_t> t2(keys, values, 14);

	t2.walk(rbt::Show<size_t, size_t>(std::cout, "\n"));

	std::cout << std::endl;

	for (size_t i = 0; i < 14; ++i)
		t2.remove(keys[i]);

	t2.walk(rbt::Show<size_t, size_t>(std::cout, "\n"));

	std::cout << std::endl;
}
