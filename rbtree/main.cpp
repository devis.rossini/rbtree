#include "rbtree.h"

//#include "rbtree_basic_test.h"
#include "bench_rbtree_suite.h"

int main(int argc, const char** argv)
{
	size_t sz		= 1000;
	size_t times	= 10;

	//if (argc > 1)
	//{
	//	sz		= atol(argv[1]);
	//}

	//if (argc > 2)
	//{
	//	times	= atol(argv[2]);
	//}

	//BasicTest();
	BasicBenchmark(sz, times);

	return 0;
}