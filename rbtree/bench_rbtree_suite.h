#ifndef BENCH_RBTREE_SUITE_H_INCLUDE_GUARD
#define BENCH_RBTREE_SUITE_H_INCLUDE_GUARD

#include "timeit.h"

#include <map>
#include <string>
#include <iostream>

#define RBTREE rbt::rbtree<size_t, size_t>

typedef std::map<std::string, double> bench_map;

void bench_creation(const size_t sz, RBTREE& t)
{
	for (size_t i = 0; i < sz; ++i)
	{
		t.insert(i, i);
	}
}

bench_map bench_once(const size_t sz)
{
	bench_map m;
	RBTREE t;
	TIME_IT(bench_creation(sz, t), m);
	return m;
}

void add_results(bench_map& m, size_t prevs, bench_map& more)
{
	for (bench_map::const_iterator it = more.begin(); it != more.end(); ++it)
	{
		bench_map::iterator ins;
		if ((ins = m.find(it->first)) != m.end())
		{
			ins->second = ((ins->second * prevs) + it->second) / (prevs + 1);
		}
	}
}

bench_map bench_some(size_t sz, size_t times)
{
	bench_map results = bench_once(sz);
	for (size_t i = 1; i < times; ++i)
	{
		bench_map tmp = bench_once(sz);
		add_results(results, i, tmp);
	}
	return results;
}

void print_bench_map(bench_map& m, std::ostream& out)
{
	for (bench_map::const_iterator it = m.begin(); it != m.end(); ++it)
	{
		out << it->first << " : "
			<< std::fixed
			<< it->second << " seconds."
			<< std::endl;
	}
}

void BasicBenchmark(size_t sz, size_t times)
{
	bench_map m = bench_some(sz, times);

	print_bench_map(m, std::cout);
}

#endif // BENCH_RBTREE_SUITE_H_INCLUDE_GUARD