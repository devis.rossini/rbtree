#ifndef TIMEIT_H_INCLUDE_GUARD
#define TIMEIT_H_INCLUDE_GUARD

#include <ctime>

#define TIME_IT(statement, m)										\
{																	\
	clock_t start = clock(), end;									\
	statement;														\
	end = clock();													\
	m[#statement] = (end - start) / (double) CLOCKS_PER_SEC;		\
}

#define TIME_IT_MEAN(statement, times, m)							\
{																	\
	clock_t start, end;												\
	double diff = 0;												\
	size_t counter;													\
	for (counter = 0; counter < times; ++counter)					\
	{																\
		start = clock();											\
		statement;													\
		end = clock();												\
		diff += (end - start);										\
	}																\
	m[#statement] = diff / (times * CLOCKS_PER_SEC);				\
}

#endif // TIMEIT_H_INCLUDE_GUARD